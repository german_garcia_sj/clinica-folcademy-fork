package com.folcademy.clinica.Exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> defaultErrorHandler(MultipartHttpServletRequest req, Exception e){ //exp generica
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Error Generico",e.getMessage(),"1",req.getRequestURI()),HttpStatus.INTERNAL_SERVER_ERROR);
    } //forma que devuelve un Json
    @ExceptionHandler(NotFoundException.class)//usar la clase definida por nosotros, no genericas de sprint
    @ResponseBody
    public ResponseEntity<ErrorMessage> notFoundHandler(HttpServletRequest req, Exception e){ //para desafío 4 //exp de archivo no encontrado (Status 404)
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Not Found",e.getMessage(),"2",req.getRequestURI()),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)//usar la clase definida por nosotros, no genericas de sprint
    @ResponseBody
    public ResponseEntity<ErrorMessage> badRequestHandler(HttpServletRequest req, Exception e){ //para desafío 4 //exp de archivo no encontrado
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Bad Request", e.getMessage(),"3",req.getRequestURI()),HttpStatus.BAD_REQUEST);
        //ej min 12 mentoria (Status 400)
    }
    @ExceptionHandler(ValidationException.class)//usar la clase definida por nosotros, no genericas de sprint
    @ResponseBody
    public ResponseEntity<ErrorMessage> ValidationException(HttpServletRequest req, Exception e){ //para desafío 4 //exp de archivo no encontrado
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("ValidationException", e.getMessage(),"4",req.getRequestURI()),HttpStatus.UNPROCESSABLE_ENTITY);
        //ej min 12 mentoria (Status 400)
    }
}
//arrojar una excepción cuando no se encuentra el registro. (NotFoundException)
// por validación de un dato (ValidationException)
//501. NotImplemented indica que el servidor no admite la función solicitada.
//err 422 Unprocessable Entity La solicitud estaba bien formada, pero no se pudo seguir debido a errores semánticos.
//err 400 Bad Request El servidor no puede o no procesará la solicitud debido a un aparente error del cliente (p. Ej., Sintaxis de solicitud mal formada, tamaño demasiado grande, encuadre de mensaje de solicitud no válido o enrutamiento de solicitud engañoso)