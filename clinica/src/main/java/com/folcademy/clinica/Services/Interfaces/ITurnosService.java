package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.TurnoDto;

import java.util.List;

public interface ITurnosService {
    List<TurnoDto> listarTodos();
    TurnoDto agregar(TurnoDto entity);
    TurnoDto modificarTurno(TurnoDto entity);

}
