package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service ("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }
    public List<MedicoDto> listarTodos(){
        if(medicoRepository.count()<1)
            throw new ValidationException ("BD incompleta");//ok
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }
    public MedicoDto listarUno(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe el medico");//ok
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
        //devuelve null sino existe
    }
    public MedicoDto agregar(MedicoDto entity){
        entity.setId(null);
        if(entity.getConsulta() <0)
            throw new ValidationException("No puede haber consulta menor q 0") ;//ok
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }
   //desafio 3
   public MedicoDto modificarUno(MedicoDto entity){
        if (!medicoRepository.existsById(entity.getId())) //sino lo encuentra
            throw new ValidationException ("Id incorrecto - No encontrado");//ok
       MedicoDto medicoAux = medicoRepository.findById(entity.getId()).orElse(null);
       medicoAux.setNombre(entity.getNombre());
       medicoAux.setApellido(entity.getApellido());
       medicoAux.setProfesion(entity.getProfesion());
       medicoAux.setConsulta(entity.getConsulta());
       return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(medicoAux)));
   }

   public String borrarUno(Integer id){
       if(!medicoRepository.existsById(id))
           throw new NotFoundException("Id no existe en los registros - No se puede borrar");//ok
        medicoRepository.deleteById(id); //medicoRepository.deleteById(id);
        return "Id Borrado:"+id;
    }
}
