package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service ("pacienteService")
public class PacienteService{
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

        public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }
    public List<PacienteDto> listarTodos(){
            if(pacienteRepository.count()<0)
                throw new NotFoundException("BD no cumple con la cantidad minima de pacientes diarios");//ok
        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }
    public PacienteDto listarUno(Integer id){
        if(!pacienteRepository.existsById(id))
            throw new ValidationException("Paciente no existe");//ok
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
        //devuelve null sino existe
    }
    public PacienteDto agregar(PacienteDto entity){
        entity.setIdpaciente(null);
        if(entity.getNombre()==""){
            throw new ValidationException ("Debe completar el nombre");}//ok
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }
    //desafio 3
    public PacienteDto modificarUno(PacienteDto entity){
        if (entity.getApellido()=="")
            throw new ValidationException ("No se ingreso un Apellido");//ok
        PacienteDto pacienteAux = pacienteRepository.findById(entity.getIdpaciente()).orElse(null);
        pacienteAux.setDni(entity.getDni());
        pacienteAux.setNombre(entity.getNombre());
        pacienteAux.setApellido(entity.getApellido());
        pacienteAux.setTelefono(entity.getTelefono());
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(pacienteAux)));
    }

    public String borrarUno(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Paciente No existe - No es viable la solicitud");//ok
        pacienteRepository.deleteById(id); //medicoRepository.deleteById(id);
        return "Id Borrado:"+id;
    }
}

