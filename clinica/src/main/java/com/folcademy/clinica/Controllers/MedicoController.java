package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/medicos") //es como es la URL del llamado
public class MedicoController{
    private final MedicoService medicoService; //ALT+ENT

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }
    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping("") //vacio para q devuelva todos los almacenados //dif min 19
    public ResponseEntity<List<MedicoDto>> listarTodo() {
        return ResponseEntity.ok(medicoService.listarTodos());
    }
    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping(value = "/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.listarUno(id));
    }
    @PreAuthorize("hasAuthority('post_medico')")
    @PostMapping("") //el objeto va por la URL
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoDto entity){
        //@RequestBody recibe en el cuerpo de la URL
        // @validated controla los datos sino rechaza el llamado (caja negra)
        return ResponseEntity.ok(medicoService.agregar(entity));
    }
    //desafio 3
    @PreAuthorize("hasAuthority('put_medico')")
    @PutMapping("")  //modificar
    public ResponseEntity<MedicoDto> modificarUno(@RequestBody @Validated MedicoDto entity){
        return ResponseEntity.ok(medicoService.modificarUno(entity));
    }//ok
    @PreAuthorize("hasAuthority('delete_medico')")
    @DeleteMapping(value = "/{idMedico}") //borrar
    public ResponseEntity<String> borrarUno(@PathVariable (name = "idMedico") Integer id){
        return ResponseEntity.ok(medicoService.borrarUno(id));//ok
    }
}
