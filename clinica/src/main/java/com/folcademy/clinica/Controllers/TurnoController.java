package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/turnos") //es como es la URL del llamado
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {

        this.turnoService = turnoService;
    }
    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping("") //vacio para q devuelva todos los almacenados
    public ResponseEntity<List<TurnoDto>> listarTodos() {
        return ResponseEntity.ok(turnoService.listarTodos());
    }

    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity<TurnoDto> listarUno(@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.listarUno(id));
    }
    @PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("") //el objeto va por la URL
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity) {
        //@RequestBody recibe en el cuerpo de la URL
        // @validated controla los datos sino rechaza el llamado (caja negra)
        return ResponseEntity.ok(turnoService.agregar(entity));
    }
    @PreAuthorize("hasAuthority('delete_turno')")
    @DeleteMapping(value = "/{idturno}") //borrar
    public ResponseEntity<String> cancelarturno(@PathVariable (name = "idturno") Integer id){
        return ResponseEntity.ok(turnoService.cancelarturno(id));//ok
    }
    @PreAuthorize("hasAuthority('put_turno')")
    @PutMapping("")  //modificar
    public ResponseEntity<TurnoDto> modificarTurno(@RequestBody @Validated TurnoDto entity){
        return ResponseEntity.ok(turnoService.modificarTurno(entity));
    }//ok
}