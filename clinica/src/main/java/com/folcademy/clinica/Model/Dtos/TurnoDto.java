package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.time.LocalTime;


@Data //declara todos getter y setters en una sola linea
@AllArgsConstructor //crea constructor
@NoArgsConstructor  //para enviar datos vacíos
public class TurnoDto {

    Integer id;
    LocalDate fecha;
    LocalTime hora;
    //@NotNull
    boolean atendido;
    Integer idp;
    Integer idm;
    Paciente paciente;
    Medico medico;
}