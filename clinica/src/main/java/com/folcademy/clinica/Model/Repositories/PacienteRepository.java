package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository ("PacienteRepository")
public interface PacienteRepository extends JpaRepository<Paciente, Integer> {

    //Page<Paciente> findAllByApellido(String apellido, Pageable pageable);
    //Codigo para metodologia sin DTO
    //Ejecuta un SELECT * FROM Paciente where Apellido = valor apellido
    // Pageable indica indice y tamaño de pagina a traer
}
