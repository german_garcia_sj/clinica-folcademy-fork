package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turnos;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
        public TurnoDto entityToDto(Turnos entity){
            return Optional  //caminos o desvios y devuelve algun objeto definido como opcional
                    .ofNullable(entity) //verifica que el obj entity no sea nulo para seguir el flujo sino sigue en orElse
                    .map(
                            ent -> new TurnoDto( //landa: nueva funcion
                                    ent.getId(),    //parametros en el mismo orden que el DTO
                                    ent.getFecha(),
                                    ent.getHora(),
                                    ent.isAtendido(),
                                    ent.getIdp(),
                                    ent.getIdm(),
                                    ent.getPaciente(),
                                    ent.getMedico()
                            )
                    )              //
                    .orElse(new TurnoDto());
        }
        public Turnos dtoToEntity(TurnoDto dto){
            Turnos entity = new Turnos(); //ya trae cadena vacia (ver declaracion Entities Medico)
            entity.setId(dto.getId()) ;
            entity.setFecha(dto.getFecha());
            entity.setHora(dto.getHora());
            entity.setAtendido(dto.isAtendido());
            entity.setIdp(dto.getIdp());
            entity.setIdm(dto.getIdm()) ;
            entity.setPaciente(dto.getPaciente());
            entity.setMedico(dto.getMedico());
            return entity;
        }
}
