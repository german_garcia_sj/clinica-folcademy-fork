package com.folcademy.clinica.Model.Repositories;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turnos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("TurnoRepository")
public interface TurnoRepository extends JpaRepository<Turnos, Integer>  {


}
